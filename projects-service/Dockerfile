FROM golang:1.13 as builder
LABEL maintainer="Santiago De Los Rios <sdelosrios95@gmail.com>"

#Adding SSH Key for pulling the repo
ARG SSH_PRIVATE_KEY

WORKDIR /lightist/
COPY . /lightist/

#Creating SSH private keys to pull the record
RUN mkdir -p ~/.ssh && umask 0700 && echo "${SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa \
	&& git config --global url."git@gitlab.com:".insteadOf https://gitlab.com/ \
	&& ssh-keyscan gitlab.com >> ~/.ssh/known_hosts \
	&& chmod 600 /root/.ssh/id_rsa

#build project
RUN CGO_ENABLED=0 GO111MODULE=on GOARCH=amd64 GOSUMDB=off go build -o projects ./main.go

#Final image
FROM alpine:latest

EXPOSE 6767

#install neccesary components
RUN apk --update add \
    curl \
		tzdata \
		ca-certificates \
		&& rm -rf /var/cache/apk/*


COPY --from=builder /lightist/projects .

#run binary executable
ENTRYPOINT ["/projects"]