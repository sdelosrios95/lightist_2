package util

import (
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
)

//BadRequestErrorHandler - Sens a 400 HTTP error to the response
func BadRequestErrorHandler(data []byte, w http.ResponseWriter) {
	errorMsg := "Error parsing thre provided request. Please check and try again"

	logrus.Println(errorMsg)
	WriteJSONResponse(w, http.StatusBadRequest, []byte(errorMsg))
}

//ServerErrorHandler Sends a 500 HTTP error to the response
func ServerErrorHandler(err error, w http.ResponseWriter) {
	if err != nil {
		logrus.Println("Server Error: %v", err.Error())
		WriteJSONResponse(w, http.StatusInternalServerError, []byte("{\"result\":\""+err.Error()+"\"}"))
	}
}

//WriteJSONResponse Writes a response with a status code data
func WriteJSONResponse(w http.ResponseWriter, status int, data []byte) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(data)))
	w.Header().Set("Connection", "close")
	w.WriteHeader(status)
	w.Write(data)
}
