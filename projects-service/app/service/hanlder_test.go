package service

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	internalModel "gitlab.com/sdelosrios95/lightist_2/projects-service/app/model"
	"gitlab.com/sdelosrios95/lightist_2/projects-service/cmd"
	"gopkg.in/h2non/gock.v1"
)

var serviceName = "projects-service"

func TestGetProjects(t *testing.T) {
	db, mock, err := sqlmock.New()

	if err != nil {
		logrus.WithError(err).Fatal("Error trying to connect to MySQL database")
	}

	//Mock database for GET /projects
	rows := mock.NewRows([]string{"id", "name", "color", "color_code"}).
		AddRow(1, "Personal", "Red", "#FF0000").
		AddRow(2, "Familiar", "Blue", "#00FF00")

	mock.ExpectQuery(`SELECT [^WHERE]`).WillReturnRows(rows)

	server := setup(db)

	defer gock.Off()

	gock.New("localhost:6767").
		Get("/projects").
		Reply(200).
		BodyString(`{"result":[{"id":"1","name":"Personal","color":"Red","colorCode":"#FF0000"},{"id":"2","name":"Familiar","color":"Blue","colorCode":"#00FF00"}]`)

	req := httptest.NewRequest("GET", "/projects", nil)
	res := httptest.NewRecorder()

	server.router.ServeHTTP(res, req)

	//Should return 200
	assert.Equal(t, 200, res.Code, "Should return an status HTTP 200 OK")

	projectResults := internalModel.ProjectsData{}
	_ = json.Unmarshal(res.Body.Bytes(), &projectResults)

	//Assert first item returned
	assert.Equal(t, internalModel.Project{ID: "1", Name: "Personal", Color: "Red", ColorCode: "#FF0000"}, projectResults.Result[0], "The first object of the result does not match")
	assert.Equal(t, internalModel.Project{ID: "2", Name: "Familiar", Color: "Blue", ColorCode: "#00FF00"}, projectResults.Result[1], "The second object of the result does not match")
}
func TestGetIdProject(t *testing.T) {
	db, mock, err := sqlmock.New()

	if err != nil {
		logrus.WithError(err).Fatal("Error trying to connect to MySQL database")
	}

	rows := mock.NewRows([]string{"id", "name", "color", "color_code"}).
		AddRow(3, "Freelance", "Green", "#0000FF")

	mock.ExpectQuery(`WHERE p.id=?`).
		WithArgs("3").
		WillReturnRows(rows)

	server := setup(db)

	defer gock.Off()

	gock.New("localhost:6767").
		Get("/projects/3").
		Reply(200).
		BodyString(`{"result":{"id":"3","name":"Freelance","color":"Green","colorCode":"#0000FF"}`)

	req := httptest.NewRequest("GET", "/projects/3", nil)
	res := httptest.NewRecorder()

	server.router.ServeHTTP(res, req)

	//Should return 200
	assert.Equal(t, 200, res.Code, "Should return an status HTTP 200 OK")

	projectData := internalModel.ProjectData{}
	_ = json.Unmarshal(res.Body.Bytes(), &projectData)

	assert.Equal(t, internalModel.Project{ID: "3", Name: "Freelance", Color: "Green", ColorCode: "#0000FF"}, projectData.Result, "The object of the result does not match")

}
func TestPostProject(t *testing.T) {
	db, mock, err := sqlmock.New()

	if err != nil {
		logrus.WithError(err).Fatal("Error trying to connect to MySQL database")
	}

	mock.ExpectPrepare("INSERT INTO projects").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	server := setup(db)

	defer gock.Off()

	gock.New("localhost:6767").
		Post("/projects").
		Reply(201).
		BodyString(`{"id":"1","name":"Freelance","color":"Green","colorCode":"#0000FF"}`)

	req := httptest.NewRequest("POST", "/projects", strings.NewReader(`{"name":"Freelance","color":"Green","colorCode":"#0000FF"}`))
	res := httptest.NewRecorder()

	server.router.ServeHTTP(res, req)

	assert.Equal(t, 201, res.Code, "Should return an status HTTP 201 OK")

	project := internalModel.Project{}
	_ = json.Unmarshal(res.Body.Bytes(), &project)

	assert.Equal(t, internalModel.Project{ID: "1", Name: "Freelance", Color: "Green", ColorCode: "#0000FF"}, project, "The expected object does not match with the response object")
}

func TestPutProject(t *testing.T) {
	db, mock, err := sqlmock.New()

	if err != nil {
		logrus.WithError(err).Fatal("Error trying to connect to MySQL database")
	}

	_ = mock.NewRows([]string{"id", "name", "color", "color_code"}).
		AddRow(3, "Freelance", "Green", "#0000FF")

	mock.ExpectExec("UPDATE projects as p").WillReturnResult(sqlmock.NewResult(0, 1))
	server := setup(db)

	defer gock.Off()

	gock.New("localhost:6767").
		Put("/projects/3").
		Reply(200).
		BodyString("Project updated successfully")

	req := httptest.NewRequest("PUT", "/projects/3", strings.NewReader(`{"name":"Familiar2","color":"red","colorCode":"#FF0000"}`))
	res := httptest.NewRecorder()

	server.router.ServeHTTP(res, req)

	assert.Equal(t, 200, res.Code, "Should return an status HTTP 200 OK")
	assert.Equal(t, "Project updated successfully", string(res.Body.Bytes()), "The result message does not match")
}

func TestDeleteProject(t *testing.T) {
	db, mock, err := sqlmock.New()

	if err != nil {
		logrus.WithError(err).Fatal("Error trying to connect to MySQL database")
	}

	_ = mock.NewRows([]string{"id", "name", "color", "color_code"}).
		AddRow(3, "Freelance", "Green", "#0000FF")

	mock.ExpectExec("DELETE FROM projects as p").WillReturnResult(sqlmock.NewResult(0, 1))
	server := setup(db)

	defer gock.Off()

	gock.New("localhost:6767").
		Delete("/projects/3").
		Reply(200).
		BodyString("Project deleted successfully")

	req := httptest.NewRequest("DELETE", "/projects/3", nil)
	res := httptest.NewRecorder()

	server.router.ServeHTTP(res, req)

	assert.Equal(t, 200, res.Code, "Should return an status HTTP 200 OK")
	assert.Equal(t, "Project deleted successfully", string(res.Body.Bytes()), "The result message does not match")

}

//Setup Server, Hanlder and Mock Database for UTs.
func setup(db *sql.DB) *Server {
	client := &http.Client{}

	gock.InterceptClient(client)

	s := NewServer(cmd.DefaultConfiguration(), NewHandler(client, db))
	s.SetupRoutes()

	return s
}
