package service

import (
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/sdelosrios95/lightist_2/projects-service/cmd"
)

//Server - struct for Project service server
type Server struct {
	configuration *cmd.Config
	router        *chi.Mux
	handler       *Handler
}

//NewServer - function to create a new server
func NewServer(config *cmd.Config, handler *Handler) *Server {
	return &Server{configuration: config, handler: handler}
}

//Close - closes the connection to server
func (s *Server) Close() {

}

//Start - starts a new server
func (s *Server) Start() {
	logrus.Infof("Starting Project service HTTP server on %v", ":"+s.configuration.Port)

	err := http.ListenAndServe(":"+s.configuration.Port, s.router)

	if err != nil {
		logrus.WithError(err).Fatal("error starting project service HTTP server")
	}
}

//SetupRoutes - function that setups all the routes of the service
func (s *Server) SetupRoutes() {
	s.router = chi.NewRouter()
	s.router.Use(middleware.RequestID)
	s.router.Use(middleware.RealIP)
	s.router.Use(middleware.Logger)
	s.router.Use(middleware.Recoverer)
	s.router.Use(middleware.Timeout(time.Minute))

	s.router.Route("/projects", func(r chi.Router) {
		r.Get("/", s.handler.GetProjects)
		r.Get("/{id}", s.handler.GetProject)
		r.Post("/", s.handler.PostProject)
		r.Put("/{id}", s.handler.PutProject)
		r.Delete("/{id}", s.handler.DeleteProject)
	})
}
