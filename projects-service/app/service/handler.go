package service

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/getsentry/sentry-go"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	internalModel "gitlab.com/sdelosrios95/lightist_2/projects-service/app/model"
	utils "gitlab.com/sdelosrios95/lightist_2/projects-service/common/util"
)

//Handler struct for endpoints handlers
type Handler struct {
	client *http.Client
	db     *sql.DB
}

//NewHandler - Creates a new handler
func NewHandler(client *http.Client, db *sql.DB) *Handler {
	return &Handler{client: client, db: db}
}

//GetProjects - Handler method for GET /projects
func (h *Handler) GetProjects(w http.ResponseWriter, r *http.Request) {

	projects, err := h.getProjects()

	if err != nil {
		sentry.CaptureException(err)
		utils.ServerErrorHandler(err, w)
		return
	}

	data, _ := json.Marshal(projects)
	utils.WriteJSONResponse(w, http.StatusOK, data)

}

//GetProject - Hanlder method for GET /projects/{id}
func (h *Handler) GetProject(w http.ResponseWriter, r *http.Request) {
	projectID := chi.URLParam(r, "id")

	if projectID == "" {
		utils.WriteJSONResponse(w, http.StatusBadRequest, []byte("id parameter is missing"))
		return
	}

	project, err := h.getProject(projectID)

	if err != nil {
		sentry.CaptureException(err)
		utils.ServerErrorHandler(err, w)
		return
	}

	if project.ID == "" {
		utils.WriteJSONResponse(w, http.StatusNotFound, []byte("Project with the provided ID not found"))
		return
	}

	projectData := internalModel.ProjectData{Result: project}

	data, _ := json.Marshal(projectData)

	utils.WriteJSONResponse(w, http.StatusOK, data)

}

//PostProject - Handler for POST /projects
func (h *Handler) PostProject(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		utils.BadRequestErrorHandler(data, w)
		return
	}

	projectData := internalModel.Project{}
	err = json.Unmarshal(data, &projectData)

	if err != nil {
		utils.BadRequestErrorHandler(data, w)
		return
	}

	err = h.createProject(&projectData)
	utils.ServerErrorHandler(err, w)

	postbody, _ := json.Marshal(&projectData)
	utils.WriteJSONResponse(w, 201, postbody)
}

//PutProject - Handler for PUT /projects
func (h *Handler) PutProject(w http.ResponseWriter, r *http.Request) {
	projectID := chi.URLParam(r, "id")

	if projectID == "" {
		utils.WriteJSONResponse(w, http.StatusBadRequest, []byte("id parameter is missing"))
		return
	}

	logrus.Println(projectID)
	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		utils.BadRequestErrorHandler(data, w)
		return
	}

	projectData := internalModel.Project{}

	err = json.Unmarshal(data, &projectData)

	if err != nil {
		utils.BadRequestErrorHandler(data, w)
		return
	}

	projectData.ID = projectID

	err = h.updateProject(&projectData)

	if err != nil {
		sentry.CaptureException(err)
		utils.ServerErrorHandler(err, w)
		return
	}

	utils.WriteJSONResponse(w, http.StatusOK, []byte("Project updated successfully"))
}

//DeleteProject - Handler for DELETE /projects/{id}
func (h *Handler) DeleteProject(w http.ResponseWriter, r *http.Request) {
	projectID := chi.URLParam(r, "id")

	if projectID == "" {
		utils.WriteJSONResponse(w, http.StatusBadRequest, []byte("id parameter is missing"))
		return
	}

	logrus.Println(projectID)
	err := h.deleteProject(projectID)

	if err != nil {
		utils.ServerErrorHandler(err, w)
		return
	}

	utils.WriteJSONResponse(w, http.StatusOK, []byte("Project deleted successfully"))
}

//Inner method for getting an specific project
func (h *Handler) getProject(projectID string) (internalModel.Project, error) {
	project := internalModel.Project{}
	results, err := h.db.Query("SELECT * FROM projects as p WHERE p.id=?", projectID)

	if err != nil {
		return project, err
	}

	defer results.Close()

	if results.Next() {
		results.Scan(&project.ID, &project.Name, &project.Color, &project.ColorCode)
	}

	return project, nil
}

//Inner method for getting all projects
func (h *Handler) getProjects() (internalModel.ProjectsData, error) {
	projectsData := internalModel.ProjectsData{}

	query, err := h.db.Query("SELECT * FROM projects as p")

	if err != nil {
		return projectsData, err
	}

	defer query.Close()

	for query.Next() {
		project := internalModel.Project{}
		query.Scan(&project.ID, &project.Name, &project.Color, &project.ColorCode)

		projectsData.Result = append(projectsData.Result, project)
	}

	return projectsData, nil
}

//Inner method for post projects
func (h *Handler) createProject(projectData *internalModel.Project) error {
	query, err := h.db.Prepare("INSERT INTO projects (name, color, color_code) VALUES (?, ?, ?)")

	if err != nil {
		return err
	}

	result, err := query.Exec(projectData.Name, projectData.Color, projectData.ColorCode)

	if err != nil {
		return err
	}

	id, err := result.LastInsertId()

	if err != nil {
		return err
	}
	defer query.Close()

	projectData.ID = strconv.Itoa(int(id))

	return nil
}

func (h *Handler) updateProject(projectData *internalModel.Project) error {
	_, err := h.db.Exec("UPDATE projects as p SET name = ?, color = ?, color_code = ? WHERE id = ?", projectData.Name, projectData.Color, projectData.ColorCode, projectData.ID)

	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) deleteProject(projectID string) error {
	_, err := h.db.Exec("DELETE FROM projects as p where p.id = ?", projectID)

	if err != nil {
		return err
	}

	return nil
}
