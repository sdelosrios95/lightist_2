package model

// The Project defines types only it knows about.

// Project defines the model
type Project struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Color     string `json:"color"`
	ColorCode string `json:"colorCode"`
}

// ProjectsData response struct for GET /projects
type ProjectsData struct {
	Result []Project `json:"result"`
}

//ProjectData response struc for GET /project/{id}
type ProjectData struct {
	Result Project `json:"result"`
}

// ToString is a somewhat generic ToString method.
func (a *Project) ToString() string {
	return a.ID + " " + a.Name
}
