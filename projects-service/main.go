package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/sdelosrios95/lightist_2/projects-service/app/service"
	"gitlab.com/sdelosrios95/lightist_2/projects-service/cmd"

	arg "github.com/alexflint/go-arg"
	"github.com/getsentry/sentry-go"
	_ "github.com/go-sql-driver/mysql"
	logrus "github.com/sirupsen/logrus"
)

var appName = "projects-service"

func main() {
	// setting up Sentry
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://60659067ac374180ba6438ad8c334bce@o403644.ingest.sentry.io/5266533",
	})

	if err != nil {
		logrus.WithError(err).Fatal("Error trying to connect to Sentry: %s", err)
	}

	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.Infof("Starting %v\n", appName)

	defaultConfiguration := cmd.DefaultConfiguration()
	arg.MustParse(defaultConfiguration)

	client := &http.Client{}

	var transport http.RoundTripper = &http.Transport{
		DisableKeepAlives: true,
	}

	client.Transport = transport

	db, err1 := sql.Open("mysql", "projects-user:rf1ljg3fl7bj2loc@tcp(lightist2-prod-do-user-7766002-0.a.db.ondigitalocean.com:25060)/projects_service")

	if err1 != nil {
		log.Printf("entra")
		logrus.WithError(err1).Fatal("Error trying to connect to MySQL database")
	}

	handler := service.NewHandler(client, db)

	s := service.NewServer(defaultConfiguration, handler)
	s.SetupRoutes()

	handleSigterm(func() {
		s.Close()
	})

	s.Start()
}

// Handles Ctrl+C or most other means of "controlled" shutdown gracefully. Invokes the supplied func before exiting.
func handleSigterm(handleExit func()) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	go func() {
		<-c
		handleExit()
		os.Exit(1)
	}()
}
